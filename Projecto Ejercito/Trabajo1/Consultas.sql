	/*==============================================================*/
	/*                    CONSULTA NUMERO UNO                       */
	/*==============================================================*/
   
   /*Mostrar el histórico de misiones de los soldados por año. En una columna debe aparecer el año, 
   en otra columna debe aparecer la duración en meses de la misión, en otra columna la 
   cantidad de soldados que participaron, en otra columna el líder de escuadrón.*/
   
   Select extract (year from misiones_ejercito.fecha_fin_mision) as Fecha, 
   (extract (month from misiones_ejercito.fecha_fin_mision) - extract (month from misiones_ejercito.fecha_inicio_mision)) as Duracion_meses,
   count (line_misiones.id_mision) as Soldados_participantes,
   misiones_ejercito.lider_grupo_misiones from misiones_ejercito 
   inner join line_misiones on misiones_ejercito.id_mision = line_misiones.id_mision
   group by 1, Duracion_meses, 4
   Order by 1 Asc
   
   /*Mostrar histórico de castigo de todos los soldados. En una columna debe aparecer la descripción del tipo 
   de castigo, en otra columna cuantos soldados han sido castigados por ese motivo, en otra columna cuantos 
   superiores han emitido ese castigo, en otra columna el número total de días cumplido en sumatoria por todos 
   los castigados.*/
   
   select castigo_ejercito.motivo_castigo as Tipo_de_castigo, 
   count (castigo_ejercito.id_integrante) as Numero_soldados_castigado,
   count (superior_ejercito.id_superior) as Numeros_superiores_EC,
   sum (extract (day from castigo_ejercito.fecha_fin_castigo) - extract (day from castigo_ejercito.fecha_inicio_castigo)) as Duracion_total
   from castigo_ejercito 
   inner join superior_ejercito on superior_ejercito.id_superior = castigo_ejercito.id_superior
   group by 1
  
  
  /*Mostrar histórico de admisiones. En una columna debe aparecer la fecha de la admisión, en otra columna el 
  número de postulantes que fueron admitidos, en otra columna el número de postulantes que fueron rechazados.*/
   
   
   Select extract (year from admision_ejercito.fecha_admision) as Fecha_admison ,
   (select count (id_admision)from admision_ejercito Where estado_admision='Admitido') as Numero_admitidos,
   (select count (id_admision)from admision_ejercito Where estado_admision='Rechazados') as Numero_rechazados
   from admision_ejercito 
   group by 1
   
   /*Mostrar el histórico de salida de soldados. En una columna debe aparecer el año, en otra columna cuantas salidas 
   por renuncia hubo, en otra columna cuantas salidas por jubilación hubo, en otra columna cuantas salidas por motivos 
   vergonzoso hubo, en otra columna el número total de juntas de superiores formadas para todos los tipos de salida.*/
   
    Select extract (year from retiro_ejercito.fecha_retiro) as Fecha_retiro,
	(select count (id_integrante)from retiro_ejercito Where tipo_de_retiro='Jubilacion') as Numero_jubilados,
   	(select count (id_integrante)from retiro_ejercito Where tipo_de_retiro='Renuncia') as Numero_renuncia,
	(select count (id_integrante)from retiro_ejercito Where tipo_de_retiro='Motivos Vergonzosos') as Numero_motivos_vergonsozos,
	(select count (id_integrante)from retiro_ejercito Where investigacion_retiro='Junta Formada') as Numero_Juntas_formadas
	from retiro_ejercito
	group by 1
	
   
   
   
   
   
   
   
   
   
   
   
   