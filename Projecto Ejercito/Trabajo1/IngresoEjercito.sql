/*==============================================================*/
/* Table: TIPOS_RANGO_EJERCITO                                  */
/*==============================================================*/
INSERT INTO public.tipos_rango_ejercito(
	id_rango, rango_actual)
	VALUES 
	(1, 'Soldado'),
	(2, 'Cabo Segundo'),
	(3, 'Cabo Primero'),
	(4, 'Sargento Segundo'),
	(5, 'Sargento Primero'),
	(6, 'Suboficial Segundo'),
	(7, 'Suboficial Primero'),
	(8, 'Suboficial Mayor'),
	(9, 'Subteniente'),
	(10, 'Teniente'),
	(11, 'Capitan'),
	(12, 'Mayor'),
	(13, 'Teniente Coronel'),
	(14, 'Coronel'),
	(15, 'General de Brigada'),
	(16, 'General de Division'),
	(17, 'General de Ejercito');
	
/*==============================================================*/
/* Table: POSTULANTES_EJERCITO                                  */
/*==============================================================*/
SET DATESTYLE TO 'European';
INSERT INTO public.postulantes_ejercito(
	id_postulante, nombre_postulante, apellido_postulante, fecha_nacimiento)
	VALUES 
	(1, 'Juan Roberto', 'Gonzalez Quiñones', '03/03/1997'),
	(2, 'Benjie David', 'Caicedo Corozo', '18/10/1998'),
	(3, 'Alexis Jesus', 'Paredes Intriago', '06/05/1999'),
	(4, 'Jose Carlos', 'Arcentales Lucas', '11/02/2000'),
	(5, 'Luis Andres', 'Navarrete Perez', '10/09/2001'),
	(6, 'Pedro Alejandro', 'Delgado Gomez', '24/04/1997'),
	(7, 'Jean Carlos', 'Castillo Suarez', '14/01/1998'),
	(8, 'Jeffry Leonel', 'Adolfo Hurtado', '06/11/1999'),
	(9, 'Alex Steven', 'Ruiz Salazar', '12/03/2000'),
	(10, 'Kevin Jairo', 'Villareal Mendoza', '29/07/2001'),
	(11, 'Antony Emilio', 'Rodriguez Bello', '28/01/1997'),
	(12, 'Walter Gustavo', 'Chavez Arias', '24/05/1998'),
	(13, 'Oscar Miguel', 'Alvaro Jurado', '17/08/1999'),
	(14, 'Christian Eduardo', 'Martinez Lopez', '01/12/2000'),
	(15, 'Deivid Leonardo', 'Reyes Flores', '22/06/2001'),
	(16, 'Jefer Andres', 'Ponce Silva', '21/06/2001'),
	(17, 'Pedro Manuel', 'Arteaga Rodriguez', '20/06/2001'),
	(18, 'Kevin Alexander', 'Santana Giner', '19/06/2001'),
	(19, 'Sebastian Esteven', 'Briones Zambrano', '18/06/2001'),
	(20, 'Crishian Alberto', 'Catagua Bravo', '17/06/2001'),
	(21, 'Lionel Andres', 'Messie Rivadeneira', '16/06/2001');
	
	
/*==============================================================*/
/* Table: ENTRENAMIENTO_SOLDADO                                 */
/*==============================================================*/
SET DATESTYLE TO 'European';
INSERT INTO public.entrenamiento_soldado(
	id_entrenamiento, fecha_inicio_entrenamiento, fecha_termino_entrenamiento, calificacion)
	VALUES 		
	(1,'01/01/2019','01/03/2019','Buena'),
    (2,'01/04/2019','01/06/2019','Excelente'),
    (3,'01/07/2019','01/09/2019','Regular'),
    (4,'01/10/2019','01/12/2019','Buena'),
    (5,'01/01/2020','01/03/2020','Excelente'),
    (6,'01/04/2020','01/06/2020','Regular'),
    (7,'01/07/2020','01/09/2020','Buena'),
    (8,'01/10/2020','01/12/2020','Excelente'),
	(9,'01/01/2021','01/03/2021','Regular'),
    (10,'01/04/2021','01/06/2021','Buena'),
    (11,'01/07/2021','01/09/2021','Excelente'),
    (12,'01/10/2021','01/12/2021','Regular');
	
	
/*==============================================================*/
/* Table: MISIONES_EJERCITO                                     */
/*==============================================================*/
SET DATESTYLE TO 'European';
INSERT INTO public.misiones_ejercito(
	id_mision, tipo_mision, lider_grupo_misiones, instrumentos_militares_usados, estado_instrumento_militar, fecha_inicio_mision, fecha_fin_mision)
	VALUES 
	(1, 'Mision Civil', 'Johan Alcivar', 'Armas militares', 'Bueno', '01/01/2019', '01/02/2019'),
	(2, 'Mision Reconocimiento Fronterizo', 'Piero Hincapie', 'Autos militares', 'Regular', '01/04/2019', '01/08/2019'),
	(3, 'Mision Civil', 'Gonzalo Plata', 'Tanque de guerra', 'Obsoleto', '01/01/2020', '01/02/2020'),
	(4, 'Mision Reconocimiento Fronterizo', 'Gonzalo Plata', 'Armas militares', 'Excelente', '01/04/2020', '01/08/2020'),
	(5, 'Mision Civil', 'Piero Hincapie', 'Autos militares', 'Regular', '01/01/2021', '01/02/2021'),
	(6, 'Mision Reconocimiento Fronterizo', 'Johan Alcivar', 'Tanque de guerra', 'Excelente', '01/04/2021', '01/08/2021');	
	
	
/*==============================================================*/
/* Table: BATALLON_EJERCITO                                     */
/*==============================================================*/
INSERT INTO public.batallon_ejercito(
	id_batallon, nombre_militar_mando, numero_batallon, alias_batallon)
	VALUES 
	(1, 'Johan ALberto Alcivar Briones', 1, 'Cobra'),
	(2, 'Gonzalo Jordy Plata Jimenez', 2, 'Aguila'),
	(3, 'Piero Martín Hincapié Reyna', 3, 'Lagarto');
	
	
/*==============================================================*/
/* Table: ADMISION_EJERCITO                                     */
/*==============================================================*/
SET DATESTYLE TO 'European';
INSERT INTO public.admision_ejercito(
	id_admision, id_postulante, fecha_admision, estado_admision)
	VALUES 
	(1, 1, '01/11/2018', 'Admitido'),
	(2, 2, '02/11/2018', 'Admitido'),
	(3, 3, '03/11/2018', 'Admitido'),
	(4, 4, '04/11/2018', 'Admitido'),
	(5, 5, '05/11/2018', 'Rechazados'),
	(6, 6, '01/11/2018', 'Admitido'),
	(7, 7, '02/11/2018', 'Admitido'),
	(8, 8, '03/11/2018', 'Admitido'),
	(9, 9, '04/11/2018', 'Admitido'),
	(10, 10, '05/11/2018', 'Rechazados'),
	(11, 11, '01/11/2018', 'Admitido'),
	(12, 12, '02/11/2018', 'Admitido'),
	(13, 13, '03/11/2018', 'Admitido'),
	(14, 14, '04/11/2018', 'Admitido'),
	(15, 15, '05/11/2018', 'Rechazados'),
	(16, 16, '01/11/2018', 'Admitido'),
	(17, 17, '02/11/2018', 'Rechazados'),
	(18, 18, '03/11/2018', 'Admitido'),
	(19, 19, '04/11/2018', 'Admitido'),
	(20, 20, '05/11/2018', 'Rechazados'),
	(21, 21, '01/11/2018', 'Admitido');
	
	
/*==============================================================*/
/* Table: LINE_ENTRENAMIENTO                                    */
/*==============================================================*/
INSERT INTO public.line_entrenamiento(
	id_batallon, id_entrenamiento)
	VALUES 
	(1, 1),
	(1, 4),
	(1, 5),
	(1, 9),
	(2, 2),
	(2, 3),
	(2, 6),
	(2, 12),
	(3, 8),
	(3, 10),
	(3, 11),
	(3, 7);
	
	
/*==============================================================*/
/* Table: INTEGRANTE_EJERCITO                                   */
/* Para poder dejar ingresar valores nulos                      */
/*==============================================================*/

alter table integrante_ejercito alter column id_admision drop not null;


/*==============================================================*/
/* Table: INTEGRANTE_EJERCITO                                   */
/*==============================================================*/
INSERT INTO public.integrante_ejercito(
	id_integrante, id_rango, id_batallon, id_admision)
	VALUES 
	(1, 2, 1, null),
	(2, 3, 2, null),
	(3, 4, 3, null),
	(4, 5, 1, null),
	(5, 6, 2, null),
	(6, 7, 3, null),
	(7, 8, 1, null),
	(8, 9, 2, null),
	(9, 10, 3, null),
	(10, 11, 1, null),
	(11, 12, 2, null),
	(12, 13, 3, null),
	(13, 14, 1, null),
	(14, 15, 2, null),
	(15, 16, 3, null),
	(16, 17, 1, null),
	(17, 1, 2, 1),
	(18, 1, 3, 2),
	(19, 1, 1, 3),
	(20, 1, 2, 4),
	(21, 1, 3, 6),
	(22, 1, 1, 7),
	(23, 1, 2, 8),
	(24, 1, 3, 9),
	(25, 1, 1, 11),
	(26, 1, 1, 12),
	(27, 1, 2, 13),
	(28, 1, 3, 14);

	
/*==============================================================*/
/* Table: PRUEBAS_DE_RANGO                                      */
/*==============================================================*/
INSERT INTO public.pruebas_de_rango(
	id_prueba, id_rango, id_integrante, pruebas_fisicas, pruebas_teoricas, cumple_condicion)
	VALUES 
	(1, 1, 17, 10, 10, 'si'),
	(2, 1, 18, 6, 9, 'no'),
	(3, 1, 19, 9, 9, 'si'),
	(4, 1, 20, 9, 10, 'no'),
	(5, 1, 21, 7, 8, 'si');
	
	
/*==============================================================*/
/* Table: LINE_MISIONES                                         */
/*==============================================================*/
INSERT INTO public.line_misiones(
	id_integrante, id_mision)
	VALUES 
	(17, 1),
	(18, 2),
	(19, 3),
	(20, 4),
	(21, 5),
	(22, 6),
	(23, 1),
	(24, 2),
	(25, 3),
	(26, 4),
	(27, 5),
	(28, 6);


/*==============================================================*/
/* Table: SUPERIOR_EJERCITO                                     */
/*==============================================================*/
INSERT INTO public.superior_ejercito(
	id_superior, id_integrante, nombre_superior, apellido_superior)
	VALUES 
	(1, 16, 'Johan Alberto', 'Alcivar Briones'),
	(2, 15, 'Piero Martin', 'Plata Jimenez'),
	(3, 14, 'Gonzalo Jordy', 'Hincapie Reyna');
	
	
/*==============================================================*/
/* Table: CASTIGO_EJERCITO                                      */
/*==============================================================*/
SET DATESTYLE TO 'European';
INSERT INTO public.castigo_ejercito(
	id_castigo, id_integrante, id_superior, fecha_incidente_castigo, fecha_inicio_castigo, fecha_fin_castigo, motivo_castigo)
	VALUES 
	(1, 17, 1, '07/01/2021', '09/01/2021', '23/01/2021', 'Indisciplina'),
	(2, 18, 1, '05/03/2021', '06/03/2021', '13/03/2021', 'Atraso'),
	(3, 23, 2, '02/05/2021', '04/05/2021', '18/05/2021', 'Indisciplina'),
	(4, 24, 2, '16/06/2021', '20/06/2021', '27/06/2021', 'Atraso'),
	(5, 27, 3, '09/09/2021', '10/09/2021', '24/09/2021', 'Indisciplina'),
	(6, 28, 3, '13/11/2021', '14/11/2021', '21/11/2021', 'Atraso');

	
/*==============================================================*/
/* Table: RETIRO_EJERCITO                                       */
/*==============================================================*/
INSERT INTO public.retiro_ejercito(
	id_retiro, id_integrante, tipo_de_retiro, fecha_retiro, investigacion_retiro)
	VALUES 
	(1, 13, 'Jubilacion', '07/11/2021', 'Junta Formada'),
	(2, 12, 'Jubilacion', '17/11/2021', 'Junta no Formada'),
	(3, 11, 'Jubilacion', '07/10/2021', 'Junta Formada'),
	(4, 10, 'Jubilacion', '17/10/2021', 'Junta no Formada'),
	(5, 1, 'Renuncia', '06/11/2021', 'Junta Formada'),
	(6, 3, 'Renuncia', '21/11/2021', 'Junta no Formada'),
	(7, 5, 'Renuncia', '27/10/2021', 'Junta Formada'),
	(8, 7, 'Renuncia', '15/10/2021', 'Junta no Formada'),
	(9, 2, 'Motivos Vergonzosos', '30/09/2021', 'Junta Formada'),
	(10, 19, 'Motivos Vergonzosos', '23/10/2021', 'Junta no Formada'),
	(11, 20, 'Motivos Vergonzosos', '16/08/2021', 'Junta Formada'),
	(12, 21, 'Motivos Vergonzosos', '03/12/2021', 'Junta no Formada');
	

	