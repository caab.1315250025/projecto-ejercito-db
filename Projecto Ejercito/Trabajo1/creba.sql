/*==============================================================*/
/* DBMS name:      PostgreSQL 9.x                               */
/* Created on:     18/11/2021 12:30:09 a. m.                    */
/*==============================================================*/



/*==============================================================*/
/* Table: ADMISION_EJERCITO                                     */
/*==============================================================*/
create table ADMISION_EJERCITO (
   ID_ADMISION          INT4                 not null,
   ID_POSTULANTE        INT4                 not null,
   FECHA_ADMISION       DATE                 null,
   ESTADO_ADMISION      VARCHAR(20)          null,
   constraint PK_ADMISION_EJERCITO primary key (ID_ADMISION)
);

/*==============================================================*/
/* Index: ADMISION_EJERCITO_PK                                  */
/*==============================================================*/
create unique index ADMISION_EJERCITO_PK on ADMISION_EJERCITO (
ID_ADMISION
);

/*==============================================================*/
/* Index: RELATIONSHIP_10_FK                                    */
/*==============================================================*/
create  index RELATIONSHIP_10_FK on ADMISION_EJERCITO (
ID_POSTULANTE
);

/*==============================================================*/
/* Table: BATALLON_EJERCITO                                     */
/*==============================================================*/
create table BATALLON_EJERCITO (
   ID_BATALLON          INT4                 not null,
   NOMBRE_MILITAR_MANDO VARCHAR(50)          null,
   NUMERO_BATALLON      INT4                 null,
   ALIAS_BATALLON       VARCHAR(20)          null,
   constraint PK_BATALLON_EJERCITO primary key (ID_BATALLON)
);

/*==============================================================*/
/* Index: BATALLON_EJERCITO_PK                                  */
/*==============================================================*/
create unique index BATALLON_EJERCITO_PK on BATALLON_EJERCITO (
ID_BATALLON
);

/*==============================================================*/
/* Table: CASTIGO_EJERCITO                                      */
/*==============================================================*/
create table CASTIGO_EJERCITO (
   ID_CASTIGO           INT4                 not null,
   ID_INTEGRANTE        INT4                 not null,
   ID_SUPERIOR          INT4                 not null,
   FECHA_INCIDENTE_CASTIGO DATE                 null,
   FECHA_INICIO_CASTIGO DATE                 null,
   FECHA_FIN_CASTIGO    DATE                 null,
   MOTIVO_CASTIGO       VARCHAR(25)          null,
   constraint PK_CASTIGO_EJERCITO primary key (ID_CASTIGO)
);

/*==============================================================*/
/* Index: CASTIGO_EJERCITO_PK                                   */
/*==============================================================*/
create unique index CASTIGO_EJERCITO_PK on CASTIGO_EJERCITO (
ID_CASTIGO
);

/*==============================================================*/
/* Index: RELATIONSHIP_20_FK                                    */
/*==============================================================*/
create  index RELATIONSHIP_20_FK on CASTIGO_EJERCITO (
ID_SUPERIOR
);

/*==============================================================*/
/* Table: ENTRENAMIENTO_SOLDADO                                 */
/*==============================================================*/
create table ENTRENAMIENTO_SOLDADO (
   ID_ENTRENAMIENTO     INT4                 not null,
   FECHA_INICIO_ENTRENAMIENTO DATE                 null,
   FECHA_TERMINO_ENTRENAMIENTO DATE                 null,
   CALIFICACION         VARCHAR(15)          null,
   constraint PK_ENTRENAMIENTO_SOLDADO primary key (ID_ENTRENAMIENTO)
);

/*==============================================================*/
/* Index: ENTRENAMIENTO_SOLDADO_PK                              */
/*==============================================================*/
create unique index ENTRENAMIENTO_SOLDADO_PK on ENTRENAMIENTO_SOLDADO (
ID_ENTRENAMIENTO
);

/*==============================================================*/
/* Table: INTEGRANTE_EJERCITO                                   */
/*==============================================================*/
create table INTEGRANTE_EJERCITO (
   ID_INTEGRANTE        INT4                 not null,
   ID_RANGO             INT4                 not null,
   ID_BATALLON          INT4                 null,
   ID_ADMISION          INT4                 not null,
   constraint PK_INTEGRANTE_EJERCITO primary key (ID_INTEGRANTE)
);

/*==============================================================*/
/* Index: INTEGRANTE_EJERCITO_PK                                */
/*==============================================================*/
create unique index INTEGRANTE_EJERCITO_PK on INTEGRANTE_EJERCITO (
ID_INTEGRANTE
);

/*==============================================================*/
/* Index: RELATIONSHIP_6_FK                                     */
/*==============================================================*/
create  index RELATIONSHIP_6_FK on INTEGRANTE_EJERCITO (
ID_BATALLON
);

/*==============================================================*/
/* Index: RELATIONSHIP_13_FK                                    */
/*==============================================================*/
create  index RELATIONSHIP_13_FK on INTEGRANTE_EJERCITO (
ID_ADMISION
);

/*==============================================================*/
/* Index: RELATIONSHIP_14_FK                                    */
/*==============================================================*/
create  index RELATIONSHIP_14_FK on INTEGRANTE_EJERCITO (
ID_RANGO
);

/*==============================================================*/
/* Table: LINE_ENTRENAMIENTO                                    */
/*==============================================================*/
create table LINE_ENTRENAMIENTO (
   ID_BATALLON          INT4                 not null,
   ID_ENTRENAMIENTO     INT4                 not null,
   constraint PK_LINE_ENTRENAMIENTO primary key (ID_BATALLON, ID_ENTRENAMIENTO)
);

/*==============================================================*/
/* Index: LINE_ENTRENAMIENTO_PK                                 */
/*==============================================================*/
create unique index LINE_ENTRENAMIENTO_PK on LINE_ENTRENAMIENTO (
ID_BATALLON,
ID_ENTRENAMIENTO
);

/*==============================================================*/
/* Index: LINE_ENTRENAMIENTO_FK                                 */
/*==============================================================*/
create  index LINE_ENTRENAMIENTO_FK on LINE_ENTRENAMIENTO (
ID_BATALLON
);

/*==============================================================*/
/* Index: LINE_ENTRENAMIENTO2_FK                                */
/*==============================================================*/
create  index LINE_ENTRENAMIENTO2_FK on LINE_ENTRENAMIENTO (
ID_ENTRENAMIENTO
);

/*==============================================================*/
/* Table: LINE_MISIONES                                         */
/*==============================================================*/
create table LINE_MISIONES (
   ID_INTEGRANTE        INT4                 not null,
   ID_MISION            INT4                 not null,
   constraint PK_LINE_MISIONES primary key (ID_INTEGRANTE, ID_MISION)
);

/*==============================================================*/
/* Index: LINE_MISIONES_PK                                      */
/*==============================================================*/
create unique index LINE_MISIONES_PK on LINE_MISIONES (
ID_INTEGRANTE,
ID_MISION
);

/*==============================================================*/
/* Index: LINE_MISIONES2_FK                                     */
/*==============================================================*/
create  index LINE_MISIONES2_FK on LINE_MISIONES (
ID_MISION
);

/*==============================================================*/
/* Table: MISIONES_EJERCITO                                     */
/*==============================================================*/
create table MISIONES_EJERCITO (
   ID_MISION            INT4                 not null,
   TIPO_MISION          VARCHAR(40)          null,
   LIDER_GRUPO_MISIONES VARCHAR(50)          null,
   INSTRUMENTOS_MILITARES_USADOS VARCHAR(50)          null,
   ESTADO_INSTRUMENTO_MILITAR VARCHAR(20)          null,
   FECHA_INICIO_MISION  DATE                 null,
   FECHA_FIN_MISION     DATE                 null,
   constraint PK_MISIONES_EJERCITO primary key (ID_MISION)
);

/*==============================================================*/
/* Index: MISIONES_EJERCITO_PK                                  */
/*==============================================================*/
create unique index MISIONES_EJERCITO_PK on MISIONES_EJERCITO (
ID_MISION
);

/*==============================================================*/
/* Table: POSTULANTES_EJERCITO                                  */
/*==============================================================*/
create table POSTULANTES_EJERCITO (
   ID_POSTULANTE        INT4                 not null,
   NOMBRE_POSTULANTE    VARCHAR(50)          null,
   APELLIDO_POSTULANTE  VARCHAR(50)          null,
   FECHA_NACIMIENTO     DATE                 null,
   constraint PK_POSTULANTES_EJERCITO primary key (ID_POSTULANTE)
);

/*==============================================================*/
/* Index: POSTULANTES_EJERCITO_PK                               */
/*==============================================================*/
create unique index POSTULANTES_EJERCITO_PK on POSTULANTES_EJERCITO (
ID_POSTULANTE
);

/*==============================================================*/
/* Table: PRUEBAS_DE_RANGO                                      */
/*==============================================================*/
create table PRUEBAS_DE_RANGO (
   ID_PRUEBA            INT4                 not null,
   ID_RANGO             INT4                 not null,
   ID_INTEGRANTE        INT4                 not null,
   PRUEBAS_FISICAS      INT4                 null,
   PRUEBAS_TEORICAS     INT4                 null,
   CUMPLE_CONDICION     VARCHAR(5)           null,
   constraint PK_PRUEBAS_DE_RANGO primary key (ID_PRUEBA)
);

/*==============================================================*/
/* Index: PRUEBAS_DE_RANGO_PK                                   */
/*==============================================================*/
create unique index PRUEBAS_DE_RANGO_PK on PRUEBAS_DE_RANGO (
ID_PRUEBA
);

/*==============================================================*/
/* Index: RELATIONSHIP_16_FK                                    */
/*==============================================================*/
create  index RELATIONSHIP_16_FK on PRUEBAS_DE_RANGO (
ID_RANGO
);

/*==============================================================*/
/* Table: RETIRO_EJERCITO                                       */
/*==============================================================*/
create table RETIRO_EJERCITO (
   ID_RETIRO            INT4                 not null,
   ID_INTEGRANTE        INT4                 not null,
   TIPO_DE_RETIRO       VARCHAR(30)          null,
   FECHA_RETIRO         DATE                 null,
   INVESTIGACION_RETIRO VARCHAR(30)          null,
   constraint PK_RETIRO_EJERCITO primary key (ID_RETIRO)
);

/*==============================================================*/
/* Index: RETIRO_EJERCITO_PK                                    */
/*==============================================================*/
create unique index RETIRO_EJERCITO_PK on RETIRO_EJERCITO (
ID_RETIRO
);

/*==============================================================*/
/* Table: SUPERIOR_EJERCITO                                     */
/*==============================================================*/
create table SUPERIOR_EJERCITO (
   ID_SUPERIOR          INT4                 not null,
   ID_INTEGRANTE        INT4                 not null,
   NOMBRE_SUPERIOR      VARCHAR(30)          null,
   APELLIDO_SUPERIOR    VARCHAR(30)          null,
   constraint PK_SUPERIOR_EJERCITO primary key (ID_SUPERIOR)
);

/*==============================================================*/
/* Index: SUPERIOR_EJERCITO_PK                                  */
/*==============================================================*/
create unique index SUPERIOR_EJERCITO_PK on SUPERIOR_EJERCITO (
ID_SUPERIOR
);

/*==============================================================*/
/* Index: RELATIONSHIP_17_FK                                    */
/*==============================================================*/
create  index RELATIONSHIP_17_FK on SUPERIOR_EJERCITO (
ID_INTEGRANTE
);

/*==============================================================*/
/* Table: TIPOS_RANGO_EJERCITO                                  */
/*==============================================================*/
create table TIPOS_RANGO_EJERCITO (
   ID_RANGO             INT4                 not null,
   RANGO_ACTUAL         VARCHAR(20)          null,
   constraint PK_TIPOS_RANGO_EJERCITO primary key (ID_RANGO)
);

/*==============================================================*/
/* Index: TIPOS_RANGO_EJERCITO_PK                               */
/*==============================================================*/
create unique index TIPOS_RANGO_EJERCITO_PK on TIPOS_RANGO_EJERCITO (
ID_RANGO
);

alter table ADMISION_EJERCITO
   add constraint FK_ADMISION_RELATIONS_POSTULAN foreign key (ID_POSTULANTE)
      references POSTULANTES_EJERCITO (ID_POSTULANTE)
      on delete restrict on update restrict;

alter table CASTIGO_EJERCITO
   add constraint FK_CASTIGO__RELATIONS_INTEGRAN1 foreign key (ID_INTEGRANTE)
      references INTEGRANTE_EJERCITO (ID_INTEGRANTE)
      on delete restrict on update restrict;

alter table CASTIGO_EJERCITO
   add constraint FK_CASTIGO__RELATIONS_SUPERIOR foreign key (ID_SUPERIOR)
      references SUPERIOR_EJERCITO (ID_SUPERIOR)
      on delete restrict on update restrict;

alter table INTEGRANTE_EJERCITO
   add constraint FK_INTEGRAN_RELATIONS_ADMISION foreign key (ID_ADMISION)
      references ADMISION_EJERCITO (ID_ADMISION)
      on delete restrict on update restrict;

alter table INTEGRANTE_EJERCITO
   add constraint FK_INTEGRAN_RELATIONS_TIPOS_RA foreign key (ID_RANGO)
      references TIPOS_RANGO_EJERCITO (ID_RANGO)
      on delete restrict on update restrict;

alter table INTEGRANTE_EJERCITO
   add constraint FK_INTEGRAN_RELATIONS_BATALLON foreign key (ID_BATALLON)
      references BATALLON_EJERCITO (ID_BATALLON)
      on delete restrict on update restrict;

alter table LINE_ENTRENAMIENTO
   add constraint FK_LINE_ENT_LINE_ENTR_BATALLON foreign key (ID_BATALLON)
      references BATALLON_EJERCITO (ID_BATALLON)
      on delete restrict on update restrict;

alter table LINE_ENTRENAMIENTO
   add constraint FK_LINE_ENT_LINE_ENTR_ENTRENAM foreign key (ID_ENTRENAMIENTO)
      references ENTRENAMIENTO_SOLDADO (ID_ENTRENAMIENTO)
      on delete restrict on update restrict;

alter table LINE_MISIONES
   add constraint FK_LINE_MIS_LINE_MISI_INTEGRAN foreign key (ID_INTEGRANTE)
      references INTEGRANTE_EJERCITO (ID_INTEGRANTE)
      on delete restrict on update restrict;

alter table LINE_MISIONES
   add constraint FK_LINE_MIS_LINE_MISI_MISIONES foreign key (ID_MISION)
      references MISIONES_EJERCITO (ID_MISION)
      on delete restrict on update restrict;

alter table PRUEBAS_DE_RANGO
   add constraint FK_PRUEBAS__RELATIONS_INTEGRAN foreign key (ID_INTEGRANTE)
      references INTEGRANTE_EJERCITO (ID_INTEGRANTE)
      on delete restrict on update restrict;

alter table PRUEBAS_DE_RANGO
   add constraint FK_PRUEBAS__RELATIONS_TIPOS_RA foreign key (ID_RANGO)
      references TIPOS_RANGO_EJERCITO (ID_RANGO)
      on delete restrict on update restrict;

alter table RETIRO_EJERCITO
   add constraint FK_RETIRO_E_RELATIONS_INTEGRAN foreign key (ID_INTEGRANTE)
      references INTEGRANTE_EJERCITO (ID_INTEGRANTE)
      on delete restrict on update restrict;

alter table SUPERIOR_EJERCITO
   add constraint FK_SUPERIOR_RELATIONS_INTEGRAN foreign key (ID_INTEGRANTE)
      references INTEGRANTE_EJERCITO (ID_INTEGRANTE)
      on delete restrict on update restrict;

