/*============================================================================*/
/*  Un procedimiento almacenado al que se le ingrese el nombre de un batallón */
/*	y que retorne todos los soldados pertenecientes al mismo.                 */
/*============================================================================*/
create or replace function batallones(character varying) returns setof "record"
as
$body$
	select batallon_ejercito.alias_batallon, postulantes_ejercito.nombre_postulante, postulantes_ejercito.apellido_postulante 
	from batallon_ejercito
	inner join integrante_ejercito on integrante_ejercito.id_batallon = batallon_ejercito.id_batallon
	inner join line_misiones on integrante_ejercito.id_integrante = line_misiones.id_integrante
	inner join admision_ejercito on admision_ejercito.id_admision = integrante_ejercito.id_admision
	inner join postulantes_ejercito on postulantes_ejercito.id_postulante = admision_ejercito.id_postulante
	where batallon_ejercito.alias_batallon = $1;
$body$
language sql;

select * from batallones('Aguila')
as ("nombre_batallon" character varying, "nombre_postulante" character varying, "apellido_postulante" character varying);
